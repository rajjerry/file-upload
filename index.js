const Express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = Express();
app.use(bodyParser.json());

app.use(cookieParser());
var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./Images");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});

var upload = multer({
    storage: Storage,
    limits: {
        fileSize: 5 * 1024 * 1024 // no larger than 5mb.
    }
}).array("imgUploader", 3);

app.get("/", function(req, res) {
    res.sendFile(__dirname + "/index.html");
    console.log('Cookies not signed: ', req.cookies);
    console.log('Signed Cookies: ', req.signedCookies)

});

app.listen(8080, function(a) {
    console.log("Listening to port 8080");
});

app.post("/Upload", function(req, res) {
    upload(req, res, function(err) {
        if (err) {
            console.log(err.message);
            res.status(500).send({ status: 500, message: err.message });
        }
        return res.end("File uploaded sucessfully!.");
    });
});